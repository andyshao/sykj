﻿@{
    ViewData["Title"] = "编辑{Comment}";
    Layout = "~/Areas/Manager/Views/Shared/_Layout.cshtml";
}
@model Sykj.Entity.{ModelName}
<div class="layui-bg">
    <form class="layui-form" style="padding: 20px 30px 0 0;" method="post">
	{FromList}
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit lay-filter="edit" id="submit" type="button">提交</button>
                <button id="close" type="button" class="layui-btn layui-btn-primary">关闭</button>
            </div>
        </div>
    </form>
</div>
@section scriptsfeet
{
    <script type="text/javascript">
    layui.config({
        base: '/layuiadmin/' //静态资源所在路径;
    }).use(['form','sykjwh'], function () {

        var $ = layui.$,form = layui.form,sykjwh = layui.sykjwh;

        $("#close").click(function () {
            sykjwh.close();
        });

        //表单提交
        form.on('submit(edit)', function (obj) {
            sykjwh.save('@Url.Action("edit", "{ModelName}", new { area = Constant.AREAMANAGER })', obj.field);
        });
    });
    </script>
}
