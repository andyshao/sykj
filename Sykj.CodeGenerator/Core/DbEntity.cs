﻿using MySql.Data.MySqlClient;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;

namespace Sykj.CodeGenerator
{
    /// <summary>
    /// 数据库转实体
    /// </summary>
    public class DbEntity
    {
        DatabaseType _databaseType;
        string _connectionString;
        DbProviderFactory Factory;
        DbConnection DbConnection;

        /// <summary>
        /// 数据库名称
        /// </summary>
        public string DataBaseName { get; set; }

        #region 构造函数

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="databaseType">数据库类型</param>
        /// <param name="connectionString">数据库连接</param>
        public DbEntity(DatabaseType databaseType, string connectionString)
        {
            _databaseType = databaseType;
            _connectionString = connectionString;

            switch (_databaseType)
            {
                case DatabaseType.SqlServer:
                    Factory = SqlClientFactory.Instance;
                    break;
                case DatabaseType.MySQL:
                    Factory = MySqlClientFactory.Instance;
                    break;
                case DatabaseType.PostgreSQL:
                    Factory = NpgsqlFactory.Instance;
                    break;
                default:
                    throw new ArgumentNullException($"还不支持的{_databaseType.ToString()}数据库类型");
            }

            DbConnection = Factory.CreateConnection();
            DbConnection.ConnectionString = connectionString;
            DataBaseName = DbConnection.Database;
        }

        #endregion

        /// <summary>
        /// 获取完整数据库信息包含表和列的信息
        /// </summary>
        /// <returns></returns>
        public List<DbTable> GetCurrentDatabaseTableList()
        {
            List<DbTable> tables = GetCurrentDatabaseAllTables();
            tables.ForEach(item =>
            {
                item.Columns = GetColumnsByTableName(item.TableName);
                item.Columns.ForEach(x =>
                {
                    var csharpType = DbColumnTypeCollection.DbColumnDataTypes.FirstOrDefault(t =>
                        t.DatabaseType == _databaseType && t.ColumnTypes.Split(',').Any(p =>
                            p.Trim().Equals(x.ColumnType, StringComparison.OrdinalIgnoreCase)))?.CSharpType;
                    if (string.IsNullOrEmpty(csharpType))
                    {
                        throw new SqlTypeException($"未从字典中找到\"{x.ColumnType}\"对应的C#数据类型，请更新DbColumnTypeCollection类型映射字典。");
                    }
                    x.CSharpType = csharpType;
                });
            });
            return tables;
        }

        #region 数据库操作

        /// <summary>
        /// 执行查询语句，返回DataSet
        /// </summary>
        /// <param name="commandText">SQL语句或存储过程名</param>
        /// <returns>DataSet</returns>
        private DataSet ExecuteDataset(string commandText)
        {
            DbConnection.Open();
            try
            {
                DbCommand cmd = Factory.CreateCommand();
                cmd.Connection = DbConnection;
                cmd.CommandText = commandText;
                using (DbDataAdapter da = Factory.CreateDataAdapter())
                {
                    da.SelectCommand = cmd;
                    DataSet dt = new DataSet();
                    da.Fill(dt);
                    DbConnection.Close();
                    return dt;
                }
            }
            catch (DbException exc)
            {
                DbConnection.Close();
                throw exc;
            }
        }

        /// <summary>
        /// 将DataTable转换为list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dt"></param>
        /// <returns></returns>
        private List<T> DTtoList<T>(DataTable dt)
        {
            if (dt == null)
                return null;
            List<T> result = new List<T>();
            for (int j = 0; j < dt.Rows.Count; j++)
            {
                T _t = (T)Activator.CreateInstance(typeof(T));
                System.Reflection.PropertyInfo[] propertys = _t.GetType().GetProperties();
                foreach (System.Reflection.PropertyInfo pi in propertys)
                {
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        // 属性与字段名称一致的进行赋值 
                        if (pi.Name.ToLower().Equals(dt.Columns[i].ColumnName.ToLower()))
                        {
                            if (dt.Rows[j][i] != DBNull.Value)
                            {
                                if (pi.PropertyType.ToString() == "System.Int32")
                                {
                                    pi.SetValue(_t, Int32.Parse(dt.Rows[j][i].ToString()), null);
                                }
                                else if (pi.PropertyType.ToString() == "System.Nullable`1[System.Int32]")
                                {
                                    pi.SetValue(_t, int.Parse(dt.Rows[j][i].ToString()), null);
                                }
                                else if (pi.PropertyType.ToString() == "System.Int64")
                                {
                                    pi.SetValue(_t, Int64.Parse(dt.Rows[j][i].ToString()), null);
                                }
                                else if (pi.PropertyType.ToString() == "System.Nullable`1[System.Int64]")
                                {
                                    pi.SetValue(_t, Int64.Parse(dt.Rows[j][i].ToString()), null);
                                }
                                else if (pi.PropertyType.ToString() == "System.DateTime")
                                {
                                    pi.SetValue(_t, Convert.ToDateTime(dt.Rows[j][i]), null);
                                }
                                else if (pi.PropertyType.ToString() == "System.Boolean")
                                {
                                    pi.SetValue(_t, Convert.ToBoolean(dt.Rows[j][i]), null);
                                }
                                else if (pi.PropertyType.ToString() == "System.Single")
                                {
                                    pi.SetValue(_t, Convert.ToSingle(dt.Rows[j][i]), null);
                                }
                                else if (pi.PropertyType.ToString() == "System.Double")
                                {
                                    pi.SetValue(_t, Convert.ToDouble(dt.Rows[j][i]), null);
                                }
                                else
                                {
                                    pi.SetValue(_t, dt.Rows[j][i].ToString(), null);
                                }
                            }
                            else
                            {
                                pi.SetValue(_t, null, null);//为空，但不为Null
                            }
                            break;
                        }
                    }
                }
                result.Add(_t);
            }
            return result;
        }

        #endregion

        #region 获取各种实体集合

        /// <summary>
        /// 根据数据库类型获取数据库中所有的表
        /// </summary>
        private List<DbTable> GetCurrentDatabaseAllTables()
        {
            DataTable dt = ExecuteDataset(GetAllTablesSql()).Tables[0];
            List<DbTable> list = DTtoList<DbTable>(dt);
            return list;
        }

        /// <summary>
        /// 根据表名，获取表所有的列
        /// </summary>
        /// <param name="tableName">数据库表名</param>
        /// <returns></returns>
        private List<DbTableColumn> GetColumnsByTableName(string tableName)
        {
            DataTable dt = ExecuteDataset(GetAllColumnsSql(tableName)).Tables[0];
            List<DbTableColumn> list = DTtoList<DbTableColumn>(dt);
            return list;
        }

        #endregion

        #region 构造SQL

        /// <summary>
        /// 获取每种数据库的表集合SQL
        /// </summary>
        /// <returns></returns>
        private string GetAllTablesSql()
        {
            string strGetAllTables = string.Empty;
            switch (_databaseType)
            {
                case DatabaseType.SqlServer:
                    strGetAllTables = @"SELECT DISTINCT d.name as TableName, f.value as TableComment
FROM      sys.syscolumns AS a LEFT OUTER JOIN
                sys.systypes AS b ON a.xusertype = b.xusertype INNER JOIN
                sys.sysobjects AS d ON a.id = d.id AND d.xtype = 'U' AND d.name <> 'dtproperties' LEFT OUTER JOIN
                sys.syscomments AS e ON a.cdefault = e.id LEFT OUTER JOIN
                sys.extended_properties AS g ON a.id = g.major_id AND a.colid = g.minor_id LEFT OUTER JOIN
                sys.extended_properties AS f ON d.id = f.major_id AND f.minor_id = 0";
                    break;
                case DatabaseType.MySQL:
                    strGetAllTables =
                    "SELECT TABLE_NAME as TableName," +
                    " Table_Comment as TableComment" +
                    " FROM INFORMATION_SCHEMA.TABLES" +
                    $" where TABLE_SCHEMA = '{DbConnection.Database}'";
                    break;
                case DatabaseType.PostgreSQL:
                    strGetAllTables =
                    "select relname as TableName," +
                    " cast(obj_description(relfilenode,'pg_class') as varchar) as TableComment" +
                    " from pg_class c" +
                    " where relkind = 'r' and relname not like 'pg_%' and relname not like 'sql_%'" +
                    " order by relname";
                    break;
                default:
                    throw new ArgumentNullException($"这是我的错，还不支持的{_databaseType.ToString()}数据库类型");

            }
            return strGetAllTables;

        }

        /// <summary>
        /// 获取当前表的字段集合SQL
        /// </summary>
        /// <param name="tableName">表名</param>
        /// <returns></returns>
        private string GetAllColumnsSql(string tableName)
        {
            var strGetTableColumns = string.Empty;
            switch (_databaseType)
            {
                case DatabaseType.SqlServer:
                    strGetTableColumns = $@"SELECT   a.name AS ColName, CONVERT(bit, (CASE WHEN COLUMNPROPERTY(a.id, a.name, 'IsIdentity') 
                = 1 THEN 1 ELSE 0 END)) AS IsIdentity, CONVERT(bit, (CASE WHEN
                    (SELECT   COUNT(*)
                     FROM      sysobjects
                     WHERE   (name IN
                                         (SELECT   name
                                          FROM      sysindexes
                                          WHERE   (id = a.id) AND (indid IN
                                                              (SELECT   indid
                                                               FROM      sysindexkeys
                                                               WHERE   (id = a.id) AND (colid IN
                                                                                   (SELECT   colid
                                                                                    FROM      syscolumns
                                                                                    WHERE   (id = a.id) AND (name = a.name))))))) AND (xtype = 'PK')) 
                > 0 THEN 1 ELSE 0 END)) AS IsPrimaryKey, b.name AS ColumnType, COLUMNPROPERTY(a.id, a.name, 'PRECISION') 
                AS ColumnLength, CONVERT(bit, (CASE WHEN a.isnullable = 1 THEN 1 ELSE 0 END)) AS IsNullable, ISNULL(e.text, '') 
                AS DefaultValue, ISNULL(g.value, ' ') AS Comment
FROM      sys.syscolumns AS a LEFT OUTER JOIN
                sys.systypes AS b ON a.xtype = b.xusertype INNER JOIN
                sys.sysobjects AS d ON a.id = d.id AND d.xtype = 'U' AND d.name <> 'dtproperties' LEFT OUTER JOIN
                sys.syscomments AS e ON a.cdefault = e.id LEFT OUTER JOIN
                sys.extended_properties AS g ON a.id = g.major_id AND a.colid = g.minor_id LEFT OUTER JOIN
                sys.extended_properties AS f ON d.id = f.class AND f.minor_id = 0
WHERE   (b.name IS NOT NULL) AND (d.name = '{tableName}')
ORDER BY a.id, a.colorder";
                    break;
                case DatabaseType.MySQL:
                    strGetTableColumns =
                   "select column_name as ColName, " +
                   " column_default as DefaultValue," +
                   " IF(extra = 'auto_increment','TRUE','FALSE') as IsIdentity," +
                   " IF(is_nullable = 'YES','TRUE','FALSE') as IsNullable," +
                   " DATA_TYPE as ColumnType," +
                   " CHARACTER_MAXIMUM_LENGTH as ColumnLength," +
                   " IF(COLUMN_KEY = 'PRI','TRUE','FALSE') as IsPrimaryKey," +
                   " COLUMN_COMMENT as Comment " +
                   $" from information_schema.columns where table_schema = '{DbConnection.Database}' and table_name = '{tableName}'";
                    break;
                case DatabaseType.PostgreSQL:
                    strGetTableColumns =
                   "select column_name as ColName," +
                   "data_type as ColumnType," +
                   "coalesce(character_maximum_length, numeric_precision, -1) as ColumnLength," +
                   "CAST((case is_nullable when 'NO' then 0 else 1 end) as bool) as IsNullable," +
                   "column_default as DefaultValue," +
                   "CAST((case when position('nextval' in column_default)> 0 then 1 else 0 end) as bool) as IsIdentity, " +
                   "CAST((case when b.pk_name is null then 0 else 1 end) as bool) as IsPrimaryKey," +
                   "c.DeText as Comment" +
                   " from information_schema.columns" +
                   " left join " +
                   " (select pg_attr.attname as colname,pg_constraint.conname as pk_name from pg_constraint " +
                   " inner join pg_class on pg_constraint.conrelid = pg_class.oid" +
                   " inner join pg_attribute pg_attr on pg_attr.attrelid = pg_class.oid and  pg_attr.attnum = pg_constraint.conkey[1]" +
                   $" inner join pg_type on pg_type.oid = pg_attr.atttypid where pg_class.relname = '{tableName}' and pg_constraint.contype = 'p') b on b.colname = information_schema.columns.column_name " +
                   " left join " +
                   " (select attname, description as DeText from pg_class " +
                   " left join pg_attribute pg_attr on pg_attr.attrelid = pg_class.oid" +
                   " left join pg_description pg_desc on pg_desc.objoid = pg_attr.attrelid and pg_desc.objsubid = pg_attr.attnum " +
                   $" where pg_attr.attnum > 0 and pg_attr.attrelid = pg_class.oid and pg_class.relname = '{tableName}') c on c.attname = information_schema.columns.column_name" +
                   $" where table_schema = 'public' and table_name = '{tableName}' order by ordinal_position asc";
                    break;
                default:
                    throw new ArgumentNullException($"这是我的错，还不支持的{_databaseType.ToString()}数据库类型");

            }
            return strGetTableColumns;
        }

        #endregion
    }
}
