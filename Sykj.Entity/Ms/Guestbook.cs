/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：                                                    
*│　作    者：bjg                                           
*│　版    本：1.0   模板代码自动生成                                              
*│　创建时间：2019-02-20 11:49:47 
*│  公    司：云南尚雅科技文化有限公司
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间: Sykj.Entity                                  
*│　类    名：Guestbook                                     
*└──────────────────────────────────────────────────────────────┘
*/
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sykj.Entity
{
    /// <summary>
    /// 留言信息表
    /// </summary>
    public class Guestbook
    {
        /// <summary>
        /// 主键
        /// </summary>
        public Int32 Id { get; set; }

        /// <summary>
        /// 留言者ID
        /// </summary>
        [Required(ErrorMessage = "请输入留言者ID")]
        public Int32 UserId { get; set; }

        /// <summary>
        /// 留言者昵称
        /// </summary>
        [MaxLength(255)]
        public String NickName { get; set; }

        /// <summary>
        /// 留言标题
        /// </summary>
        [MaxLength(500)]
        public String Title { get; set; }

        /// <summary>
        /// 留言内容
        /// </summary>
        [MaxLength(65535)]
        public String Description { get; set; }

        /// <summary>
        /// 留言时间
        /// </summary>
        [Required(ErrorMessage = "请输入留言时间")]
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// 处理人昵称
        /// </summary>
        [MaxLength(255)]
        public String HandlerNickName { get; set; }

        /// <summary>
        /// 处理人Id
        /// </summary>
        public Int32? HandlerUserId { get; set; }

        /// <summary>
        /// 处理时间
        /// </summary>
        public DateTime? HandlerDate { get; set; }

        /// <summary>
        /// 隐私(0 如果登录状态不显示当前登录者信息.1公开)
        /// </summary>
        public Int32 Privacy { get; set; }

        /// <summary>
        /// 回复内容
        /// </summary>
        [MaxLength(65535)]
        public String ReplyDescription { get; set; }

        /// <summary>
        /// 父评论Id
        /// </summary>
        public Int32 ParentId { get; set; }

        /// <summary>
        /// 审核状态
        /// </summary>
        public Int32? AuditStatus{ get; set;}

    #region 扩展属性
    /// <summary>
    /// 0：留言未处理 1：留言已处理
    /// </summary>
    [Required(ErrorMessage = "请输入0：留言未处理 1：留言已处理")]
        public Int32 Status { get; set; }

        /// <summary>
        /// 浏览次数
        /// </summary>
        public Int32 BrowseCount { get; set; }

        /// <summary>
        /// 留言人用户头像
        /// </summary>
        [NotMapped]
        public string Gravatar { get; set; }

        /// <summary>
        /// 处理人头像
        /// </summary>
        [NotMapped]
        public string HandlerGravatar { get; set; }
        /// <summary>
        /// 是否关注 1 关注 2未关注
        /// </summary>
        [NotMapped]
        public bool? IsFavorite { get; set; }
        #endregion
    }
}
