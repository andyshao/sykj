﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sykj.Entity
{
    /// <summary>
    /// 栏目管理
    /// </summary>
    public class Channel
    {
        /// <summary>
        /// 栏目类别ID
        /// </summary>
        public int ChannelId { get; set; }
        /// <summary>
        /// 栏目名称
        /// </summary>
        [Display(Name = "栏目名称")]
        [Required(ErrorMessage = "此项不能为空")]
        public string ChanneName { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        [Display(Name = "排序")]
        public int Sort { get; set; }
        /// <summary>
        /// 所属父级
        /// </summary>
        [Display(Name = "所属父级")]
        public int ParentId { get; set; }
        /// <summary>
        /// 状态1启用，0禁用
        /// </summary>
        [Display(Name = "栏目状态")]
        public int Status { get; set; }
        /// <summary>
        /// 图标
        /// </summary>
        [Display(Name = "图标")]
        [FileExtensions(Extensions = ".jpg,.png", ErrorMessage = "图片格式错误")]
        public string ImageUrl { get; set; }
        /// <summary>
        /// 栏目描述
        /// </summary>
        [Display(Name = "栏目描述")]
        public string Description { get; set; }
        /// <summary>
        /// 关键词
        /// </summary>
        [Display(Name = "关键字")]
        public string Keywords { get; set; }
        /// <summary>
        /// 类别
        /// </summary>
        public int ModuleId { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateDate { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public int CreateUserId { get; set; }
        /// <summary>
        /// 路径
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// 层级
        /// </summary>
        public int? Depth { get; set; }
        /// <summary>
        /// 是否有子级
        /// </summary>
        public bool IsHasChildren { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Display(Name = "备注")]
        public string Remark { get; set; }
        /// <summary>
        /// Title
        /// </summary>
        public string MetaTitle { get; set; }
        /// <summary>
        /// Description
        /// </summary>
        public string MetaDescription { get; set; }
        /// <summary>
        /// Keywords
        /// </summary>
        public string MetaKeywords { get; set; }
    }
}
