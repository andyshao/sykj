﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sykj.IServices
{
    /// <summary>
    /// 
    /// </summary>
    public interface IPermissions : Sykj.Repository.IRepository<Sykj.Entity.Permissions>
    {
        /// <summary>
        /// 组装树
        /// </summary>
        /// <returns></returns>
        List<Sykj.ViewModel.ZTreeModel> GetTreeList();
    }
}
