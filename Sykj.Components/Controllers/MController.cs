﻿
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Security.Claims;

namespace Sykj.Components
{
    /// <summary>
    /// 后台控制器基类
    /// </summary>
    [Area(Constant.AREAMANAGER)]
    [Authorize(Policy = "Permission")]
    public class MController : BaseController
    {
        private int _userid = 0;

        /// <summary>
        /// 获取登录的用户ID,未登陆返回0
        /// </summary>
        public int UserId
        {
            get
            {
                if (User == null) return _userid;
                if (User.Identity != null)
                {
                    var claim = (User.Identity as ClaimsIdentity).Claims.SingleOrDefault(s => s.Type == ClaimTypes.Sid);
                    if (claim != null)
                        return int.Parse(claim.Value);
                }
                return _userid;
            }
            set { _userid = value; }
        }
    }
}