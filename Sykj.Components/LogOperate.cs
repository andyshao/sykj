﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Sykj.Infrastructure;

namespace Sykj.Components
{
    /// <summary>
    /// 操作日志记录
    /// </summary>
    public class LogOperate
    {
        static Sykj.IServices.ILog _log;

        static LogOperate()
        {
            _log = BaseConfig.ServiceProvider.GetService<IServices.ILog>();
        }

        /// <summary>
        /// 记录操作日志
        /// </summary>
        /// <param name="title">标题</param>
        /// <param name="detail">详细</param>
        /// <param name="url">url</param>
        /// <param name="userId">userId</param>
        /// <param name="userName">userName</param>
        public static void Add(string title, string detail, string url, string userId, string userName)
        {
            Sykj.Entity.Log logModel = new Entity.Log()
            {
                Title = title,
                Detail = detail,
                UserName = userName,
                UserId = userId,
                IpAddress = Utils.GetIp(),
                Url = url,
                CreateDate = DateTime.Now
            };
            _log.Add(logModel);
        }

        /// <summary>
        /// 记录操作日志 web
        /// </summary>
        /// <param name="title">标题</param>
        /// <param name="detail">详细</param>
        /// <param name="context">http请求信息</param>
        public static void Add(string title, string detail, HttpContext context)
        {
            Sykj.Entity.Log logModel = new Entity.Log()
            {
                Title = title,
                Detail = detail,
                UserName = context.User.Identity.GetUserName(),
                UserId = context.User.Identity.GetUserId().ToString(),
                Type = 2,
                IpAddress = Utils.GetIp(),
                Url = context.Request.GetAbsoluteUri(),
                CreateDate = DateTime.Now
            };
            _log.Add(logModel);
        }
    }
}
