﻿using Microsoft.AspNetCore.Authorization;

namespace Sykj.Components
{
    /// <summary>
    /// 必要参数类
    /// </summary>
    public class PermissionRequirement : IAuthorizationRequirement
    {
        /// <summary>
        /// 
        /// </summary>
        public Sykj.IServices.IUsers User;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="user"></param>
        public PermissionRequirement(Sykj.IServices.IUsers user)
        {
            User = user;
        }
    }
}
