﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Sykj.Infrastructure;
using System;
using System.Reflection;

namespace Sykj.Components
{
    /// <summary>
    /// 工厂
    /// </summary>
    public class Factory
    {
        /// <summary>
        /// 创建SmsServer的实例
        /// </summary>
        /// <returns></returns>
        public static ISmsServer CreateSmsServer()
        {
            string className = BaseConfig.GetValue("SmsServerCalssName");
            //程序集名称
            string assemblyString = "Sykj.Components";
            //命名空间名称
            string typeName = string.Format("{0}.{1}", assemblyString, className);

            //从缓存中获取
            Assembly assembly = Assembly.Load(assemblyString);
            object objType = assembly.CreateInstance(typeName);
            return (ISmsServer)objType;
        }

        /// <summary>
        /// 创建PushService的实例
        /// </summary>
        /// <returns></returns>
        public static PushService CreatePushService()
        {
            string className = BaseConfig.GetValue("PushServiceCalssName");
            //程序集名称
            string assemblyString = "Sykj.Components";
            //命名空间名称
            string typeName = string.Format("{0}.{1}", assemblyString, className);

            //从缓存中获取
            Assembly assembly = Assembly.Load(assemblyString);
            object objType = assembly.CreateInstance(typeName);
            return (PushService)objType;
        }

        /// <summary>
        /// 构造依赖注入容器
        /// </summary>
        /// <returns></returns>
        public static IServiceProvider BuildServiceProvider()
        {
            //创建容器
            var services = new ServiceCollection();

            #region 注入数据
            //依赖注入，取得DbContext实例 使用DbContext池，提高性能
            //------Mysql数据库------.UseLoggerFactory(new Sykj.Repository.EFLoggerFactory())
            services.AddDbContextPool<Sykj.Repository.SyDbContext>(options => options.UseMySql(Configuration.GetConnectionString("ConnectionString")));
            //------Mysql数据库------

            //注入数据
            services.AddDataService();
            #endregion

            #region 启用缓存 读取配置是否使用哪种缓存模式
            //启用dotnet本地缓存服务
            services.AddMemoryCache();

            if (Convert.ToBoolean(Configuration["IsRedis"]))
            {
                services.AddSingleton<ICacheService, RedisCacheService>();

                //将Redis分布式缓存服务添加到服务中
                services.AddDistributedRedisCache(options =>
                {
                    //用于连接Redis的配置 
                    options.Configuration = Configuration["RedisConnectionString"];
                    //Redis实例名RedisDistributedCache
                    options.InstanceName = "RedisDistributedCache";
                });
            }
            else
            {
                services.AddSingleton<ICacheService, MemoryCacheService>();
            }
            #endregion

            #region 设置系统的依赖注入的服务提供器
            //设置系统的依赖注入的服务提供器
            BaseConfig.ServiceProvider = services.BuildServiceProvider();
            #endregion

            BaseConfig.Configuration = Configuration;

            return services.BuildServiceProvider(); //构建服务提供程序
        }

        /// <summary>
        /// 获取配置的实例
        /// </summary>
        public static IConfiguration Configuration
        {
            get
            {
                var builder = new ConfigurationBuilder()
               .SetBasePath(AppContext.BaseDirectory)
               .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
               .AddEnvironmentVariables();
                return builder.Build();
            }
        }
    }
}
