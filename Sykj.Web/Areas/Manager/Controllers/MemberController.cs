﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Sykj.IServices;
using System.Text;
using Sykj.Components;

namespace Sykj.Web.Areas.Manager.Controllers
{
    /// <summary>
    /// 会员管理
    /// </summary>
    public class MemberController : MController
    {
        IUsers _users;
        IUserMember _usersMember;
        IPointsDetail _pointsDetail;

       /// <summary>
       /// 
       /// </summary>
       /// <param name="users"></param>
       /// <param name="usersMember"></param>
       /// <param name="pointsDetail"></param>
        public MemberController(IUsers users, IUserMember usersMember, IPointsDetail pointsDetail)
        {
            _users = users;
            _usersMember = usersMember;
            _pointsDetail = pointsDetail;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 绑定数据
        /// </summary>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <param name="keyWords"></param>
        /// <returns></returns>
        public IActionResult List(int page,int limit,string keyWords)
        {
            StringBuilder strWhere = new StringBuilder(" 1=1 ");
            if (!string.IsNullOrWhiteSpace(keyWords))
            {
                strWhere.Append(" AND (UserName.Contains(@0) OR TrueName.Contains(@0))");
            }

            var list = _usersMember.GetMemberList(page, limit, strWhere.ToString(), " UserId desc ", keyWords);
            int recordCount = _usersMember.GetMemberRecordCount(strWhere.ToString(), keyWords);
            return Json(ListResult(list, recordCount));
        }

        /// <summary>
        /// 编辑性别
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="sex"></param>
        /// <returns></returns>
        public IActionResult SexEdit(int userId, string sex)
        {
            var model = _usersMember.GetModel(c => c.UserId == userId);
            if (model == null)
            {
                return Json(Failed("不存在该用户"));
            }
            if (string.IsNullOrWhiteSpace(sex))
            {
                return Json(Failed("修改失败，性别信息为空"));
            }
            Sykj.Entity.Users user = new Entity.Users()
            {
                UserId = userId,
                Sex = sex
            };
            bool b = _users.Update(user, true, c => c.Sex);
            Components.LogOperate.Add("修改会员性别", "修改ID为【" + userId + "】的性别为【"+sex+"】", HttpContext);
            return Json(AjaxResult(b));
        }

        /// <summary>
        /// 批量启用
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult StartUsing(List<int> ids)
        {
            bool b=false;
            foreach (var id in ids)
            {
                var model = new Sykj.Entity.Users()
                {
                    UserId = id,
                    Activity = true
                };
                b= _users.Update(model, true, c => c.Activity);
            }
            Components.LogOperate.Add("批量启用会员", "启用ID为【" + string.Join(",", ids) + "】的会员", HttpContext);
            return Json(AjaxResult(b));
        }

        /// <summary>
        /// 批量禁用
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Forbidden(List<int> ids)
        {
            bool b = false;
            foreach (var id in ids)
            {
                var model = new Sykj.Entity.Users()
                {
                    UserId = id,
                    Activity = false
                };
                b = _users.Update(model, true, c => c.Activity);
            }
            Components.LogOperate.Add("批量禁用会员", "禁用ID为【" + string.Join(",", ids) + "】的会员", HttpContext);
            return Json(AjaxResult(b));
        }
        
        /// <summary>
        /// 查看详情视图
        /// </summary>
        /// <param name="id">会员id</param>
        /// <returns></returns>
        public IActionResult Info(int id)
        {
            var model = _usersMember.GetMemberModel(c => c.UserId == id);
            if (model == null)
            {
                return Content("查询不到数据！");
            }
            return View(model);
        }

        /// <summary>
        /// 列表页
        /// </summary>
        /// <returns></returns>
        public IActionResult PointsDetail()
        {
            return View();
        }

        /// <summary>
        /// 查询数据
        /// </summary>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <param name="keyWords"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public IActionResult PointsDetailList(int page, int limit, string keyWords,DateTime start,DateTime end)
        {
            StringBuilder strWhere = new StringBuilder(" 1=1 ");
            if (!string.IsNullOrWhiteSpace(keyWords))
            {
                strWhere.Append(" AND Description.Contains(@0)) ");
            }
            if (start != DateTime.MinValue)
            {
                strWhere.Append(" AND CreatedDate>=@1 ");
            }
            if (end != DateTime.MinValue)
            {
                strWhere.Append(" AND CreatedDate<=@2 ");
            }
            var list = _pointsDetail.GetPagedList(page, limit, strWhere.ToString(), " PointId desc ", keyWords,start,end);
            int recordCount = _pointsDetail.GetRecordCount(strWhere.ToString(), keyWords, start, end);
            return Json(ListResult(list, recordCount));
        }

    }
}