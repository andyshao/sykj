﻿
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sykj.IServices;
using Sykj.ViewModel;
using Sykj.Infrastructure;
using Sykj.Components;
using System.Linq;
using System.Collections.Generic;
using System.Security.Claims;

namespace Sykj.Web.Areas.Manager.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class HomeController : MController
    {
        IUsers _users;
        IPermissions _permissions;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="users"></param>
        /// <param name="permissions"></param>
        public HomeController(IUsers users, IPermissions permissions)
        {
            _users = users;
            _permissions = permissions;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public IActionResult Index()
        {
            
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("index", "login", Sykj.Components.Constant.AREAMANAGER);
            }

            HomeModel homeModel = new HomeModel();
            homeModel.AccountsUser = _users.GetModel(c => c.UserId == UserId);
            List<Sykj.Entity.Roles> rolesList = new List<Entity.Roles>();
            string r = User.Claims.SingleOrDefault(s => s.Type == ClaimTypes.Role).Value;
            if (!string.IsNullOrWhiteSpace(r))
            {
                rolesList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Sykj.Entity.Roles>>(r);
            }
            homeModel.PermissionsList = _users.GetPermissionsList(UserId, rolesList).OrderBy(c => c.Sort).ToList();
            return View(homeModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public IActionResult Default()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public IActionResult Noright()
        {
            return Content("无权限访问");
        }
    }
}