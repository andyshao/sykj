﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Sykj.Components;
using Sykj.IServices;
using Sykj.ViewModel;

namespace Sykj.Web.Areas.Manager.Controllers
{
    /// <summary>
    /// 广告
    /// </summary>
    public class AdController : MController
    {
        IAdvertisement _advertisement;
        IAdvertiseposition _advertiseposition;
        Sykj.Infrastructure.ICacheService _cacheService;

        /// <summary>
        /// 依赖注入
        /// </summary>
        /// <param name="advertisement">广告详情信息</param>
        /// <param name="advertiseposition">广告位信息</param>
        /// <param name="cacheService">缓存</param>
        public AdController(IAdvertisement advertisement, IAdvertiseposition advertiseposition, Sykj.Infrastructure.ICacheService cacheService)
        {
            _advertisement = advertisement;
            _advertiseposition = advertiseposition;
            _cacheService = cacheService;
        }

        #region 位置

        /// <summary>
        /// 广告位分页视图
        /// </summary>
        /// <returns></returns>
        public IActionResult AdpositionIndex()
        {
            return View();
        }

        /// <summary>
        /// 获取广告位列表
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="limit">每页显示条数</param>
        /// <param name="keyWords">查找的关键字</param>
        /// <returns></returns>
        public IActionResult AdpositionList(int page, int limit, string keyWords)
        {
            StringBuilder strWhere = new StringBuilder(" 1=1 ");
            if (!string.IsNullOrWhiteSpace(keyWords))
            {
                strWhere.Append(" AND (AdvPositionName.Contains(@0))");
            }
            var list = _advertiseposition.GetPagedList(page, limit, strWhere.ToString(), " AdvPositionId desc ", keyWords);
            int recordCount = _advertiseposition.GetRecordCount(strWhere.ToString(), keyWords);
            return Json(ListResult(list, recordCount));
        }

        /// <summary>
        /// 添加视图
        /// </summary>
        /// <returns></returns>
        public IActionResult AdpositionAdd()
        {
            return View();
        }

        /// <summary>
        /// 添加保存
        /// </summary>
        /// <param name="advertiseposition">广告位实体</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult AdpositionAdd(Sykj.Entity.Advertiseposition advertiseposition)
        {
            advertiseposition.CreateDate = DateTime.Now;
            advertiseposition.CreateUserId = UserId;
            advertiseposition.RepeatColumns = advertiseposition.RepeatColumns ?? 0;
            bool b= _advertiseposition.Add(advertiseposition);
            Components.LogOperate.Add("添加广告位", "添加ID为【" + advertiseposition.AdvPositionId + "】的广告位", HttpContext);
            return Json(AjaxResult(b));
        }

        /// <summary>
        /// 编辑视图
        /// </summary>
        /// <param name="id">广告位id</param>
        /// <returns></returns>
        public IActionResult AdpositionEdit(int id)
        {
            var model = _advertiseposition.GetModel(c => c.AdvPositionId == id);
            if (model == null)
            {
                return Content("查询不到数据！");
            }
            return View(model);
        }

        /// <summary>
        /// 编辑保存
        /// </summary>
        /// <param name="advertiseposition">广告位实体</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult AdpositionEdit(Sykj.Entity.Advertiseposition advertiseposition)
        {
            var model = _advertiseposition.GetModel(c => c.AdvPositionId == advertiseposition.AdvPositionId);
            if (model == null)
            {
                return Json(Failed("查询不到该数据"));
            }
            model.AdvPositionName = advertiseposition.AdvPositionName;
            model.ShowType = advertiseposition.ShowType;
            model.IsOne = advertiseposition.IsOne;
            model.TimeInterval = advertiseposition.TimeInterval;
            bool b = _advertiseposition.Update(model);
            Components.LogOperate.Add("编辑广告位", "编辑ID为【" + advertiseposition.AdvPositionId + "】的广告位", HttpContext);
            return Json(AjaxResult(b));
        }

        /// <summary>
        /// 批量删除广告位
        /// </summary>
        /// <param name="ids">id的集合</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult AdpositionDeleteAll(List<int> ids)
        {
            bool b = _advertiseposition.Delete(ids);
            Components.LogOperate.Add("批量删除广告位", "删除ID为【" + string.Join(",", ids) + "】的广告位", HttpContext);
            return Json(AjaxResult(b));
        }

        #endregion

        #region 广告内容

        /// <summary>
        /// 广告内容视图
        /// </summary>
        /// <param name="advPositionId">广告位id</param>
        /// <returns></returns>
        public IActionResult AdvertisementIndex(int advPositionId)
        {
            ViewBag.AdvPositionId = advPositionId;
            return View();
        }

        /// <summary>
        /// 绑定广告内容数据
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="limit">每页数据条数</param>
        /// <param name="keyWords">搜索关键字</param>
        /// <param name="advPositionId">广告位id</param>
        /// <returns></returns>
        public IActionResult AdvertisementList(int page, int limit, string keyWords, int advPositionId)
        {
            var model = _advertiseposition.GetModel(c => c.AdvPositionId == advPositionId);
            if (model == null)
            {
                return Json(Failed("查询不到该数据"));
            }
            //将advPositionId作为查询条件
            StringBuilder strWhere = new StringBuilder(" AdvPositionId = "+ advPositionId);
            if (!string.IsNullOrWhiteSpace(keyWords))
            {
                strWhere.Append(" AND (AdvertisementName.Contains(@0) OR AlternateText.Contains(@0))");
            }
            var list = _advertisement.GetPagedList(page, limit, strWhere.ToString(), " AdvertisementId desc ", keyWords);
            int recordCount = _advertisement.GetRecordCount(strWhere.ToString(), keyWords);
            return Json(ListResult(list, recordCount));
        }

        /// <summary>
        /// 添加广告内容的视图
        /// </summary>
        /// <param name="id">广告位id</param>
        /// <returns></returns>
        public IActionResult AdvertisementAdd(int id)
        {
            ViewBag.AdvPositionId = id;
            return View();
        }

        /// <summary>
        /// 添加广告内容
        /// </summary>
        /// <param name="advertisement">广告详情的实体</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult AdvertisementAdd(Sykj.Entity.Advertisement advertisement)
        {
            advertisement.CreateDate = DateTime.Now;
            advertisement.CreateUserId = UserId;
            bool b = _advertisement.Add(advertisement);
            _cacheService.RemoveCache(Infrastructure.CacheKey.ADVERTISEMENTALL);
            Components.LogOperate.Add("添加广告内容", "添加ID为【" + advertisement.AdvertisementId + "】的广告内容", HttpContext);
            return Json(AjaxResult(b));
        }

        /// <summary>
        /// 获取编辑视图
        /// </summary>
        /// <param name="id">广告内容的id</param>
        /// <returns></returns>
        public IActionResult AdvertisementEdit(int id)
        {
            var model = _advertisement.GetModel(c => c.AdvertisementId == id);
            if (model == null)
            {
                return Content("查询不到数据！");
            }
            return View(model);
        }

        /// <summary>
        /// 保存编辑内容
        /// </summary>
        /// <param name="advertisement">广告内容的实体类</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult AdvertisementEdit(Sykj.Entity.Advertisement advertisement)
        {
            var model = _advertisement.GetModel(c => c.AdvertisementId == advertisement.AdvertisementId);
            if (model == null)
            {
                return Json(Failed("查询不到该数据"));
            }
            model.AdvertisementName = advertisement.AdvertisementName;
            model.ContentType = advertisement.ContentType;
            model.FileUrl = advertisement.FileUrl;
            model.AlternateText = advertisement.AlternateText;
            model.OperationType = advertisement.OperationType;
            model.TargetId = advertisement.TargetId;
            model.NavigateUrl = advertisement.NavigateUrl;
            model.AdvHtml = advertisement.AdvHtml;
            model.Status = advertisement.Status;
            model.StartDate = advertisement.StartDate;
            model.EndDate = advertisement.EndDate;
            model.Impressions = advertisement.Impressions;
            model.DayMaxPv = advertisement.DayMaxPv;
            model.DayMaxIp = advertisement.DayMaxIp;
            model.AutoStop = advertisement.AutoStop;
            bool b = _advertisement.Update(model);
            _cacheService.RemoveCache(Infrastructure.CacheKey.ADVERTISEMENTALL);
            Components.LogOperate.Add("编辑广告内容", "编辑ID为【" + advertisement.AdvertisementId + "】的广告内容", HttpContext);
            return Json(AjaxResult(b));
        }

        /// <summary>
        /// 修改sort值
        /// </summary>
        /// <param name="advertisementId">广告内容id</param>
        /// <param name="sort">顺序参数</param>
        /// <param name="field">字段名</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult AdvertisementSortEdit(int advertisementId,int sort,string field)
        {
            var model = _advertisement.GetModel(c => c.AdvertisementId == advertisementId);

            if (model == null)
            {
                return Json(Failed("查询不到数据！"));
            }

            if (field == "sort")
            {
                model.Sort = sort;
            }

            bool b = _advertisement.Update(model);

            Components.LogOperate.Add("编辑广告内容的排序值", "编辑ID为【" + advertisementId + "】的广告内容的排序值为【"+ sort + "】", HttpContext);

            return Json(AjaxResult(b));
        }

        /// <summary>
        /// 批量删除广告内容
        /// </summary>
        /// <param name="ids">id的集合</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult AdvertisementDeleteAll(List<int> ids)
        {
            var list = _advertisement.GetList(c => ids.Contains(c.AdvertisementId));
            bool b = _advertisement.Delete(list);
            Components.LogOperate.Add("批量删除广告内容", "删除ID为【" + string.Join(",", ids) + "】的广告内容", HttpContext);
            return Json(AjaxResult(b));
        }
        #endregion
    }
}