﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Sykj.Components;
using Sykj.IServices;

namespace Sykj.Web.Areas.Manager.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class MsController : MController
    {
        IThumbnailSize _thumbnailSize;
        IHotKeyWords _hotKeyWords;

        /// <summary>
        /// 依赖注入
        /// </summary>
        /// <param name="thumbnailSize"></param>
        /// <param name="hotKeyWords"></param>
        public MsController(IThumbnailSize thumbnailSize, IHotKeyWords hotKeyWords)
        {
            _thumbnailSize = thumbnailSize;
            _hotKeyWords = hotKeyWords;
        }

        /// <summary>
        /// 缩略图管理视图
        /// </summary>
        /// <returns></returns>
        public IActionResult ThumbnailSizeIndex()
        {
            return View();
        }

        /// <summary>
        /// 绑定缩略图数据
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="limit">每页数据条数</param>
        /// <param name="keyWords">关键字</param>
        /// <returns></returns>
        public IActionResult ThumbnailSizeList(int page, int limit, string keyWords)
        {
            StringBuilder strWhere = new StringBuilder(" 1=1 ");
            if (!string.IsNullOrWhiteSpace(keyWords))
            {
                strWhere.Append(" AND (Type=(@0))");
            }
            var list = _thumbnailSize.GetPagedList(page, limit, strWhere.ToString(), " ThumId desc ", keyWords);
            int recordCount = _thumbnailSize.GetRecordCount(strWhere.ToString(), keyWords);
            return Json(ListResult(list, recordCount));
        }


        /// <summary>
        /// 缩略图添加视图
        /// </summary>
        /// <returns></returns>
        public IActionResult ThumbnailSizeAdd()
        {
            return View();
        }

        /// <summary>
        /// 缩略图添加保存
        /// </summary>
        /// <param name="thumbnailSize">缩略图内容实体</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult ThumbnailSizeAdd(Sykj.Entity.ThumbnailSize thumbnailSize)
        {
            thumbnailSize.CreateDate = DateTime.Now;
            thumbnailSize.CreateUserID = UserId;
            bool b = _thumbnailSize.Add(thumbnailSize);
            Components.LogOperate.Add("添加缩略图", "添加ID为【" + thumbnailSize.ThumId + "】的缩略图", HttpContext);
            return Json(AjaxResult(b));
        }

        /// <summary>
        /// 删除缩略图
        /// </summary>
        /// <param name="ids">缩略图id的集合</param>
        /// <returns></returns>
        public IActionResult ThumbnailSizeDelete(List<int> ids)
        {
            var list = _thumbnailSize.GetList(c => ids.Contains(c.ThumId));
            bool b = _thumbnailSize.Delete(list);
            Components.LogOperate.Add("批量删除缩略图", "删除ID为【" + string.Join(",", ids) + "】的缩略图", HttpContext);
            return Json(AjaxResult(b));
        }

        /// <summary>
        /// 编辑缩略图的视图
        /// </summary>
        /// <param name="id">要编辑的缩略图的id</param>
        /// <returns></returns>
        public IActionResult ThumbnailSizeEdit(int id)
        {
            var model = _thumbnailSize.GetModel(c => c.ThumId == id);
            if (model == null)
            {
                return Content("查询不到数据！");
            }
            return View(model);
        }

        /// <summary>
        /// 缩略图编辑保存
        /// </summary>
        /// <param name="thumbnailSize">缩略图实体</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult ThumbnailSizeEdit(Sykj.Entity.ThumbnailSize thumbnailSize)
        {
            var model = _thumbnailSize.GetModel(c => c.ThumId == thumbnailSize.ThumId);
            if (model == null)
            {
                return Json(Failed("查询不到该数据"));
            }
            model.Type = thumbnailSize.Type;
            model.ThumWidth = thumbnailSize.ThumWidth;
            model.ThumHeight = thumbnailSize.ThumHeight;
            model.ThumMode = thumbnailSize.ThumMode;
            model.Remark = thumbnailSize.Remark;
            bool b = _thumbnailSize.Update(model);
            Components.LogOperate.Add("编辑缩略图", "编辑ID为【" + thumbnailSize.ThumId + "】的缩略图", HttpContext);
            return Json(AjaxResult(b));
        }

        #region 热门关键词
        /// <summary>
        /// 列表页
        /// </summary>
        /// <returns></returns>
        public IActionResult HotKeyWordsIndex()
        {
            return View();
        }

        /// <summary>
        /// 查询数据
        /// </summary>
        /// <param name="page">当前页</param>
        /// <param name="limit">页大小</param>
        /// <param name="keyWordsSearch">关键字</param>
        /// <returns></returns>
        public IActionResult HotKeyWordsList(int page, int limit, string keyWordsSearch)
        {
            StringBuilder strWhere = new StringBuilder(" 1=1 ");
            if (!string.IsNullOrWhiteSpace(keyWordsSearch))
            {
                strWhere.Append(" AND KeyWords.Contains(@0)) ");
            }

            var list = _hotKeyWords.GetPagedList(page, limit, strWhere.ToString(), " Id desc ", keyWordsSearch).ToList();
            int recordCount = _hotKeyWords.GetRecordCount(strWhere.ToString(), keyWordsSearch);
            return Json(ListResult(list, recordCount));
        }

        /// <summary>
        /// 添加保存
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ModelValidation]//服务器表单验证
        public IActionResult HotKeyWordsAdd(Sykj.Entity.HotKeyWords model)
        {
            bool b = _hotKeyWords.Add(model);
            Components.LogOperate.Add("添加热门关键词", "添加ID为【" + model.Id + "】的热门关键词", HttpContext);
            return Json(AjaxResult(b));
        }

        /// <summary>
        /// 编辑
        /// </summary>
        /// <returns></returns>
        public IActionResult HotKeyWordsEdit(int id)
        {
            var model = _hotKeyWords.GetModel(c => c.Id == id);
            if (model == null)
            {
                return Content("查询不到数据！");
            }
            return View(model);
        }

        /// <summary>
        /// 编辑保存
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ModelValidation]//服务器表单验证
        public IActionResult HotKeyWordsEdit(Sykj.Entity.HotKeyWords hotkeywords)
        {
            var model = _hotKeyWords.GetModel(c => c.Id == hotkeywords.Id);
            if (model == null)
            {
                return Json(Failed("查询不到该数据"));
            }
            model.CategoryId = hotkeywords.CategoryId;
            model.KeyWords = hotkeywords.KeyWords;
            bool b = _hotKeyWords.Update(model);
            Components.LogOperate.Add("编辑热门关键词", "编辑ID为【" + model.Id + "】的热门关键词", HttpContext);
            return Json(AjaxResult(b));
        }

        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult HotKeyWordsDeleteAll(List<int> ids)
        {
            var list = _hotKeyWords.GetList(c => ids.Contains(c.Id));
            bool b = _hotKeyWords.Delete(list);
            Components.LogOperate.Add("批量删除热门关键词", "删除ID为【" + string.Join(",", ids) + "】的热门关键词", HttpContext);
            return Json(AjaxResult(b));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IActionResult CategoriesTreeList()
        {
            return View();
        }

        #endregion
    }
}
