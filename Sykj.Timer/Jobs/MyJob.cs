﻿using Quartz;
using System;
using System.Threading.Tasks;

namespace Sykj.Timer
{
    public class MyJob: IJob
    {
        public async Task Execute(IJobExecutionContext context)
        {
            await Task.Run(() =>
            {
                Console.WriteLine("hello quartz!");
                //JobDetail的key就是job的分组和job的名字
                Console.WriteLine($"JobDetail的组和名字：{context.JobDetail.Key}");
                Console.WriteLine();
            });
        }

        public static IJobDetail GetJobDetail()
        {
            return JobBuilder.Create<MyJob>()                     //获取JobBuilder
                            .WithIdentity("job2")  //添加Job的名字和分组
                            .WithDescription("任务描述")     //添加描述
                            .Build();                            //生成IJobDetail
        }

        public static ITrigger GetTrigger()
        {
           return TriggerBuilder.Create()                      //获取TriggerBuilder
                              .StartAt(DateBuilder.TodayAt(01, 00, 00))  //开始时间，今天的1点（hh,mm,ss），可使用StartNow()
                              .ForJob(GetJobDetail())                               //将触发器关联给指定的job
                              .WithPriority(10)                          //优先级，当触发时间一样时，优先级大的触发器先执行
                              .WithIdentity("t2")          //添加名字和分组
                              .WithSimpleSchedule(x => x.WithIntervalInSeconds(1) //调度方案，周期 一秒执行一次
                                                        .WithRepeatCount(3)       //重复执行次数，-1为无限次
                                                        .Build()).Build();
        }
    }
}
