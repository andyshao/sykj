/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：                                                    
*│　作    者：bjg                                            
*│　版    本：1.0    模板代码自动生成                                                
*│　创建时间：2019-02-21 09:38:35       
*│  公    司：云南尚雅科技文化有限公司
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Sykj.Services                                  
*│　类    名： Favorite                                 
*└──────────────────────────────────────────────────────────────┘
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace Sykj.Services
{
    /// <summary>
    /// 收藏信息表
    /// </summary>
    public class Favorite : Sykj.Repository.RepositoryBase<Sykj.Entity.Favorite>, Sykj.IServices.IFavorite
    {
        public Favorite(Sykj.Repository.SyDbContext dbcontext) : base(dbcontext)
        {

        }

        /// <summary>
        /// 分页查询扩展(问题) bjg
        /// </summary>
        /// <param name="pageIndex">当前页</param>
        /// <param name="pageSize">页大小</param>
        /// <param name="predicate">条件</param>
        /// <param name="ordering">排序</param>
        /// <param name="args">参数</param>
        /// <returns></returns>
        public IQueryable<Entity.Favorite> GetGuestBookPagedList(int pageIndex, int pageSize, string predicate, string ordering, params object[] args)
        {
            var result = from a in _dbContext.Favorites
                         join b in _dbContext.Guestbook on a.TargetId equals b.Id
                         join c in _dbContext.Users on b.UserId equals c.UserId into d 
                         from c in d.DefaultIfEmpty()
                         select new
                         Sykj.Entity.Favorite()
                         {
                             //收藏信息表
                             FavoriteId = a.FavoriteId,
                             Type = a.Type,
                             TargetId = a.TargetId,
                             UserId = a.UserId,
                             Tags = a.Tags,
                             Remark = a.Remark,
                             CreatedDate = a.CreatedDate,
                             //专业信息表
                             GuestbookId = a.GuestbookId,
                             Gravatar = c.Gravatar,
                             NickName = b.NickName,
                             Description = b.Description
                         };
            if (!string.IsNullOrWhiteSpace(predicate))
                result = result.Where(predicate, args);
            if (!string.IsNullOrWhiteSpace(ordering))
                result = result.OrderBy(ordering);
            return result.Skip((pageIndex - 1) * pageSize).Take(pageSize);
        }
    }
}