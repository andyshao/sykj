/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：                                                    
*│　作    者：bjg                                            
*│　版    本：1.0    模板代码自动生成                                                
*│　创建时间：2019-02-20 11:49:47       
*│  公    司：云南尚雅科技文化有限公司
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Sykj.Services                                  
*│　类    名： Guestbook                                 
*└──────────────────────────────────────────────────────────────┘
*/
using System;
using System.Collections.Generic;
using System.Text;

namespace Sykj.Services
{
	/// <summary>
	/// 留言信息表
	/// </summary>
    public class Guestbook : Sykj.Repository.RepositoryBase<Sykj.Entity.Guestbook>,Sykj.IServices.IGuestbook
    {
        public Guestbook(Sykj.Repository.SyDbContext dbcontext) : base(dbcontext)
        {
		    
        }
    }
}