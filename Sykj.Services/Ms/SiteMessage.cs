/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：                                                    
*│　作    者：lh                                            
*│　版    本：1.0    模板代码自动生成                                                
*│　创建时间：2019-03-08 17:30:16       
*│  公    司：云南尚雅科技文化有限公司
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Sykj.Services                                  
*│　类    名： Sitemessage                                 
*└──────────────────────────────────────────────────────────────┘
*/
using Microsoft.EntityFrameworkCore;
using Sykj.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sykj.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class SiteMessage : Sykj.Repository.RepositoryBase<Sykj.Entity.SiteMessage>, Sykj.IServices.ISiteMessage
    {
        Sykj.IServices.IUsers _users;
        public SiteMessage(Sykj.Repository.SyDbContext dbcontext, ICacheService cacheService) : base(dbcontext)
        {
            _users = new Services.Users(dbcontext, cacheService);
        }

        /// <summary>
        /// 设置某个用户某类消息为已读
        /// </summary>
        /// <param name="msgType">消息类型</param>
        /// <param name="receiverId">接收人</param>
        /// <returns></returns>
        public bool UpdateIsRead(string msgType, int receiverId)
        {
            string strSql = "update ms_sitemessage set ReceiverIsRead=1 where MsgType={0} and ReceiverID={1}";
            int rows = _dbContext.Database.ExecuteSqlCommand(strSql, msgType, receiverId);
            return rows > 0;
        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool Add(Sykj.Entity.SiteMessage model, ref string errorMsg)
        {
            //创建事务
            var tran = _dbContext.Database.BeginTransaction();
            try
            {
                var receiverUserNameList = model.ReceiverUserNameList;
                foreach (var item in receiverUserNameList)
                {
                    //克隆对象
                    Entity.SiteMessage itemModel = model.Clone();
                    //为克隆出来的对象的接收者id赋值，并将克隆的结果写入数据库
                    var userModel = _users.GetModel(c => c.UserName == item);
                    if (userModel != null)
                    {
                        itemModel.ReceiverID = userModel.UserId;
                        _dbContext.SiteMessage.Add(itemModel);
                        _dbContext.SaveChanges();
                    }
                    else
                    {
                        errorMsg += "不存在用户" + item;
                        throw new Exception();
                    }
                }
                tran.Commit();
                return true;
            }
            catch (Exception e)
            {
                tran.Rollback();
                return false;
            }
        }
    }
}