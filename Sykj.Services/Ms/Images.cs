﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace Sykj.Services
{
    /// <summary>
    /// 图片
    /// </summary>
    public class Images : Sykj.Repository.RepositoryBase<Sykj.Entity.Images>, Sykj.IServices.IImages
    {
        public Images(Sykj.Repository.SyDbContext dbcontext) : base(dbcontext)
        {

        }
    }
}
