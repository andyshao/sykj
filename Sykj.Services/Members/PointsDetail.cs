/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：积分明细                                                    
*│　作    者：ydp                                            
*│　版    本：1.0    模板代码自动生成                                                
*│　创建时间：2019-03-06 14:26:00       
*│  公    司：云南尚雅科技文化有限公司
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Sykj.Services                                  
*│　类    名： Pointsdetail                                 
*└──────────────────────────────────────────────────────────────┘
*/
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sykj.Services
{
	/// <summary>
	/// 积分明细
	/// </summary>
    public class PointsDetail : Sykj.Repository.RepositoryBase<Sykj.Entity.PointsDetail>,Sykj.IServices.IPointsDetail
    {
        public PointsDetail(Sykj.Repository.SyDbContext dbcontext) : base(dbcontext)
        {
		    
        }

        #region 线下积分充值
        /// <summary>
        /// 线下积分充值（事务处理，需要更新个人的总积分）
        /// </summary>
        /// <param name="model">积分明细</param>
        /// <returns>添加成功返回true，失败返回false</returns>
        public bool PointsRecharge(Sykj.Entity.PointsDetail model)
        {
            //创建事务
            var tran = _dbContext.Database.BeginTransaction();
            try
            {
                //添加积分明细
                _dbContext.PointsDetail.Add(model);
                _dbContext.SaveChanges();

                //更新个人积分
                string strSql = "update accounts_usersexp set Points=Points+{0} where UserId={1}";
                _dbContext.Database.ExecuteSqlCommand(strSql, model.Score, model.UserId);
                _dbContext.SaveChanges();

                //提交事物
                tran.Commit();
                return true;
            }
            catch (Exception e)
            {
                tran.Rollback();
                return false;
            }
        }
        #endregion
    }
}