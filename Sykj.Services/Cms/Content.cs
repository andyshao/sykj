﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;

namespace Sykj.Services
{
    /// <summary>
    /// 文章内容信息
    /// </summary>
    public class Content : Sykj.Repository.RepositoryBase<Sykj.Entity.Content>, Sykj.IServices.IContent
    {
        public Content(Sykj.Repository.SyDbContext dbcontext) : base(dbcontext)
        {

        }

        #region 根据栏目ID获取指定条数记录
        /// <summary>
        /// 根据栏目ID获取指定条数记录
        /// </summary>
        /// <param name="channelId">栏目ID</param>
        /// <param name="top">提取条数</param>
        /// <param name="isDesc">是否倒序</param>
        /// <returns></returns>
        public List<Sykj.Entity.Content> GetListCid(int channelId, int top = 0, bool isDesc = true)
        {
            string order = isDesc ? " ContentId desc" : " ContentId asc ";
            return GetList(top, $"ChannelId={channelId}", order).ToList();
        }
        #endregion

        #region 分页查询
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="pageIndex">当前页</param>
        /// <param name="pageSize">页大小</param>
        /// <param name="predicate">条件</param>
        /// <param name="ordering">排序</param>
        /// <param name="args">参数</param>
        /// <returns></returns>
        public override IQueryable<Sykj.Entity.Content> GetPagedList(int pageIndex, 
            int pageSize, string predicate, string ordering, params object[] args)
        {
            var result = from a in _dbContext.Content
                         join b in _dbContext.Channel on a.ChannelId equals b.ChannelId into c
                         from b in c.DefaultIfEmpty()
                         select new
                         Sykj.Entity.Content()
                         {
                             ContentId = a.ContentId,
                             Title = a.Title,
                             SubTitle = a.SubTitle,
                             Summary = a.Summary,
                             Description = a.Description,
                             ImageUrl = a.ImageUrl,
                             ThumbImageUrl = a.ThumbImageUrl,
                             CreateDate = a.CreateDate,
                             CreateUserId = a.CreateUserId,
                             LastEditUserId = a.LastEditUserId,
                             LastEditDate = a.LastEditDate,
                             LinkUrl = a.LinkUrl,
                             PvCount = a.PvCount,
                             Status = a.Status,
                             ChannelId = a.ChannelId,
                             Keywords = a.Keywords,
                             Sort = a.Sort,
                             IsRecomend = a.IsRecomend,
                             IsHot = a.IsHot,
                             IsColor = a.IsColor,
                             IsTop = a.IsTop,
                             Remary = a.Remary,
                             TotalComment = a.TotalComment,
                             TotalSupport = a.TotalSupport,
                             TotalFav = a.TotalFav,
                             TotalShare = a.TotalShare,
                             BeFrom = a.BeFrom,
                             FileName = a.FileName,
                             MetaTitle = a.MetaTitle,
                             MetaKeywords = a.MetaKeywords,
                             MetaDescription = a.MetaDescription,
                             StaticUrl = a.StaticUrl,
                             ChannelName = b != null ? b.ChanneName : ""
                         };

            if (!string.IsNullOrWhiteSpace(predicate))
                result = result.Where(predicate, args);

            if (!string.IsNullOrWhiteSpace(ordering))
                result = result.OrderBy(ordering);

            return result.Skip((pageIndex - 1) * pageSize).Take(pageSize);
        }
        #endregion
    }
}
