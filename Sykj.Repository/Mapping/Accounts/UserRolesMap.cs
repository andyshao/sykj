﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Sykj.Repository
{
    public class UserRolesMap : IEntityTypeConfiguration<Sykj.Entity.UserRoles>
    {
        public void Configure(EntityTypeBuilder<Sykj.Entity.UserRoles> entity)
        {
            entity.HasKey(e => new { e.Id });

            entity.ToTable("accounts_userroles");

        }
    }
}
