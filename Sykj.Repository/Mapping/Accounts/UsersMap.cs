﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Sykj.Repository
{
    /// <summary>
    /// 手动配置User表映射
    /// </summary>
    public partial class UsersMap : IEntityTypeConfiguration<Sykj.Entity.Users>
    {
        public void Configure(EntityTypeBuilder<Sykj.Entity.Users> entity)
        {
            entity.HasKey(e => e.UserId);

            entity.ToTable("accounts_users");

            //entity.Property(e => e.Activity).HasColumnType("bit(1)");
        }
    }
}
