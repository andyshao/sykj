﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Sykj.Repository
{
    public class ImagesMap : IEntityTypeConfiguration<Sykj.Entity.Images>
    {
        public void Configure(EntityTypeBuilder<Sykj.Entity.Images> entity)
        {
            entity.HasKey(e => e.ImageId);

            entity.ToTable("ms_images");
        }
    }
}
