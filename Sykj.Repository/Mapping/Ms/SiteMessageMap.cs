/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：                                                    
*│　作    者：ydp                                              
*│　版    本：1.0   模板代码自动生成                                              
*│　创建时间：2019-03-08 17:30:16 
*│  公    司：云南尚雅科技文化有限公司
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间: Sykj.Repository                                  
*│　类    名：Sitemessage                                     
*└──────────────────────────────────────────────────────────────┘
*/
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Sykj.Repository
{
	/// <summary>
	/// 
	/// </summary>
	public class SiteMessageMap : IEntityTypeConfiguration<Sykj.Entity.SiteMessage>
    {
        public void Configure(EntityTypeBuilder<Sykj.Entity.SiteMessage> entity)
        {
            entity.HasKey(e => e.Id);

            entity.ToTable("ms_sitemessage");
        }
    }
}
