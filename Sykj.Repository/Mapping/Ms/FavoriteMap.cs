/**
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：                                                    
*│　作    者：bjg                                              
*│　版    本：1.0   模板代码自动生成                                              
*│　创建时间：2019-02-21 09:38:35 
*│  公    司：云南尚雅科技文化有限公司
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间: Sykj.Repository                                  
*│　类    名：Favorite                                     
*└──────────────────────────────────────────────────────────────┘
*/
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Sykj.Repository
{
    /// <summary>
    /// 收藏信息表
    /// </summary>
    public class FavoriteMap : IEntityTypeConfiguration<Sykj.Entity.Favorite>
    {
        public void Configure(EntityTypeBuilder<Sykj.Entity.Favorite> entity)
        {
            entity.HasKey(e => e.FavoriteId);

            entity.ToTable("ms_favorite");
        }
    }
}
