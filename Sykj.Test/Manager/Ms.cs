﻿using System;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Sykj.ViewModel;
using Sykj.Web.Areas.Manager.Controllers;
using Microsoft.AspNetCore.Mvc;
using Sykj.IServices;
using System.Collections.Generic;
using Sykj.Components;
using Sykj.Infrastructure;

namespace Sykj.Test.Manager
{
    /// <summary>
    /// 高校信息
    /// </summary>
    public class Ms : IDisposable
    {
        IServiceProvider _serviceProvider;
        IThumbnailSize _thumbnailSize;
        IHotKeyWords _hotKeyWords;
        MsController _msController;

        /// <summary>
        /// 构造方法
        /// </summary>
        public Ms()
        {
            _serviceProvider = Factory.BuildServiceProvider();
            _thumbnailSize = _serviceProvider.GetService<IThumbnailSize>();
            _hotKeyWords = _serviceProvider.GetService<IHotKeyWords>();
            _msController = new MsController(_thumbnailSize, _hotKeyWords);
        }

        /// <summary>
        /// 缩略图列表
        /// </summary>
        /// <param name="page">页</param>
        /// <param name="limit">页大小</param>
        /// <param name="keyWords">关键词</param>
        /// <returns></returns>
        [Theory]
        [InlineData(1,10,"")]
        public void ThumbnailSizeList(int page, int limit, string keyWords)
        {
            JsonResult r = _msController.ThumbnailSizeList(page, limit, keyWords) as JsonResult;
            ApiResult result = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());
            Assert.True(result.success);
        }

        /// <summary>
        /// 缩略图增改删测试用例
        /// </summary>
        /// <returns></returns>
        [Fact]
        public void ThumbnailSizeAddEditDelete()
        {
            //增
            Sykj.Entity.ThumbnailSize model = new Entity.ThumbnailSize();
            model.ThumName = "测试缩略图";
            model.Type = 1;
            model.ThumWidth = 100;
            model.ThumHeight = 100;
            model.ThumMode = "HW";
            model.Remark = "测试测试";
            _msController.UserId = 88;
            JsonResult r = _msController.ThumbnailSizeAdd(model) as JsonResult;
            ApiResult result1 = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());

            //改
            model.Type = 1;
            model.ThumWidth = 100;
            model.ThumHeight = 100;
            model.ThumMode = "H";
            model.Remark = "测试测试";
            r = _msController.ThumbnailSizeEdit(model) as JsonResult;
            ApiResult result2 = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());

            //删
            List<int> list = new List<int>();
            list.Add(model.ThumId);
            r = _msController.ThumbnailSizeDelete(list) as JsonResult;
            ApiResult result3 = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());

            Assert.True(result1.success&& result2.success&& result3.success);
        }

        /// <summary>
        /// 热门关键词列表
        /// </summary>
        /// <param name="page">页</param>
        /// <param name="limit">页大小</param>
        /// <param name="keyWords">关键词</param>
        /// <returns></returns>
        [Theory]
        [InlineData(1, 10, "")]
        public void HotKeyWordsList(int page, int limit, string keyWords)
        {
            JsonResult r = _msController.HotKeyWordsList(page, limit, keyWords) as JsonResult;
            ApiResult result = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());
            Assert.True(result.success);
        }

        /// <summary>
        /// 热门关键词增改删测试用例
        /// </summary>
        /// <returns></returns>
        [Fact]
        public void HotKeyWordsAddEditDelete()
        {
            //增
            Sykj.Entity.HotKeyWords model = new Entity.HotKeyWords();
            model.CategoryId = 0;
            model.KeyWords = "热门";
            _msController.UserId = 88;
            JsonResult r = _msController.HotKeyWordsAdd(model) as JsonResult;
            ApiResult result1 = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());

            //改
            model.CategoryId = 0;
            model.KeyWords = "热门233";
            r = _msController.HotKeyWordsEdit(model) as JsonResult;
            ApiResult result2 = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());

            //删
            List<int> list = new List<int>();
            list.Add(model.Id);
            r = _msController.HotKeyWordsDeleteAll(list) as JsonResult;
            ApiResult result3 = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());

            Assert.True(result1.success && result2.success && result3.success);
        }

        public void Dispose()
        {
        }
    }
}
