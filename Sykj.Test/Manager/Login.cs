﻿using System;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Sykj.ViewModel;
using Sykj.Web.Areas.Manager.Controllers;
using Microsoft.AspNetCore.Mvc;
using Sykj.IServices;
using System.Collections.Generic;
using Sykj.Components;
using Sykj.Infrastructure;

namespace Sykj.Test.Manager
{
    /// <summary>
    /// 高校信息
    /// </summary>
    public class Login : IDisposable
    {
        IServiceProvider _serviceProvider;
        IUsers _users;
        ILog _mslog;
        LoginController _loginController;

        /// <summary>
        /// 构造方法
        /// </summary>
        public Login()
        {
            _serviceProvider = Factory.BuildServiceProvider();
            _users = _serviceProvider.GetService<IUsers>();
            _mslog = _serviceProvider.GetService<ILog>();
            _loginController = new LoginController(_users, _mslog);
        }

        /// <summary>
        /// 增改删测试用例
        /// </summary>
        /// <returns></returns>
        [Fact]
        public void LoginAndLoginOut()
        {
            //增
            string username = "admin";
            string password = "admin!@#";
            var r = _users.ValidateLogin(username, password);

            Assert.True(r.success);
        }

        public void Dispose()
        {
        }
    }
}
