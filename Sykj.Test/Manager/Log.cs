﻿using Sykj.Infrastructure;
using Sykj.IServices;
using Sykj.Web.Api.v1;
using System;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Sykj.ViewModel;
using Sykj.Web.Areas.Manager.Controllers;
using Microsoft.AspNetCore.Mvc;
using Sykj.Components;

namespace Sykj.Test.Manager
{
    public class Log: IDisposable
    {
        IServiceProvider _serviceProvider;
        ILog _log;
        LogController _logController;

        public Log()
        {
            _serviceProvider = Factory.BuildServiceProvider();
            _log = _serviceProvider.GetService<ILog>();
            _logController = new LogController(_log);
        }

        /// <summary>
        /// 多场景参数测试
        /// </summary>
        [Theory]
        [InlineData(1, 10)]
        [InlineData(2, 10)]
        [InlineData(3, 10)]
        public void List(int pageIndex,int pageSize)
        {
            JsonResult r = _logController.List(pageIndex, pageSize, 1,"","","") as JsonResult;
            ApiResult result = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());
            Assert.True(result.success);
        }

        /// <summary>
        /// 清除测试数据
        /// </summary>
        public void Dispose()
        {
            //清除测试数据
        }
    }
}
