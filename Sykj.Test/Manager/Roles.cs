﻿using System;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Sykj.ViewModel;
using Sykj.Web.Areas.Manager.Controllers;
using Microsoft.AspNetCore.Mvc;
using Sykj.IServices;
using System.Collections.Generic;
using Sykj.Components;
using Sykj.Infrastructure;
using System.Linq;

namespace Sykj.Test.Manager
{
    /// <summary>
    /// 角色
    /// </summary>
    public class Roles : IDisposable
    {
        IServiceProvider _serviceProvider;
        IRoles _roles;
        IPermissions _permissions;
        RolesController _rolesController;
        ICacheService _cacheService;

        /// <summary>
        /// 构造方法
        /// </summary>
        public Roles()
        {
            _serviceProvider = Factory.BuildServiceProvider();
            _roles = _serviceProvider.GetService<IRoles>();
            _permissions = _serviceProvider.GetService<IPermissions>();
            _cacheService = _serviceProvider.GetService<ICacheService>();
            _rolesController = new RolesController(_roles, _permissions, _cacheService);
        }

        /// <summary>
        /// 列表
        /// </summary>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <param name="keyWords"></param>
        /// <returns></returns>
        [Theory]
        [InlineData(1,10,"")]
        public void List(int page, int limit, string keyWords)
        {
            JsonResult r = _rolesController.List(page, limit, keyWords) as JsonResult;
            ApiResult result = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());
            Assert.True(result.success);
        }

        ///
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Fact]
        public void RoleTree()
        {
            var r = _rolesController.RoleTree() as JsonResult;
            Assert.True(r.Value.ToString().Length>0);
        }

        ///
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Fact]
        public void PermissionsTree()
        {
            JsonResult r = _rolesController.PermissionsTree() as JsonResult;
            Assert.True(r.Value.ToString().Length > 0);
        }

        /// <summary>
        /// 增改删测试用例
        /// </summary>
        /// <returns></returns>
        [Fact]
        public void AddEditDelete()
        {
            //增
            Sykj.Entity.Roles model = new Entity.Roles();
            model.Title = "测试角色";
            model.Description = "测试角色";
            _rolesController.UserId = 3;
            JsonResult r = _rolesController.Add(model) as JsonResult;
            ApiResult result1 = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());

            //改
            model.Title = "测试角色";
            model.Description = "测试角色";
            r = _rolesController.Edit(model) as JsonResult;
            ApiResult result2 = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());

            //添加权限
            List<Sykj.Entity.Rolepermissions> rolepermissions = new List<Entity.Rolepermissions>();
            rolepermissions.Add(
                new Sykj.Entity.Rolepermissions()
                {
                    RoleId=model.RoleId,
                    PermissionId=33
                });
            r = _rolesController.AddPermissions(rolepermissions) as JsonResult;
            ApiResult result4 = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());

            //删除权限
            r = _rolesController.DeletePermissions(model.RoleId) as JsonResult;
            ApiResult result5 = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());

            //删
            List<int> list = new List<int>();
            list.Add(model.RoleId);
            r = _rolesController.DeleteAll(list) as JsonResult;
            ApiResult result3 = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());

            Assert.True(result1.success&& result2.success&& result3.success&& result4.success&& result5.success);
        }

        public void Dispose()
        {
        }
    }
}
