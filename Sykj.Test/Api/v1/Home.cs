﻿using Sykj.Infrastructure;
using Sykj.IServices;
using Sykj.Web.Api.v1;
using System;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Sykj.ViewModel;
using Sykj.Components;
namespace Sykj.Test.Api.v1
{
    public class Home : IDisposable
    {
        IServiceProvider _serviceProvider;
        IAdvertisement _advertisement;
        IRegions _newRegions;
        private readonly IContent _content;
        HomeController _homeController;
        private readonly IUsers _users;
        private readonly IUserMember _userMember;

        /// <summary>
        /// 构造方法
        /// </summary>
        public Home()
        {
            _serviceProvider = Factory.BuildServiceProvider();
            _advertisement = _serviceProvider.GetService<IAdvertisement>();
            _newRegions = _serviceProvider.GetService<IRegions>();
            _content = _serviceProvider.GetService<IContent>();
            _users = _serviceProvider.GetService<IUsers>();
            _userMember = _serviceProvider.GetService<IUserMember>();
            _homeController = new HomeController(_advertisement, _newRegions, _content, _users, _userMember);//实例化 TODO ydp注释周一来处理 
        }

        /// <summary>
        /// 首页测试
        /// </summary>
        [Fact]
        public void HomeIndex()
        {
            ApiResult result = _homeController.HomeIndex();
            Assert.True(result.success);
        }
        /// <summary>
        /// 清除数据
        /// </summary>
        public void Dispose()
        {
            //
        }
    }
}
