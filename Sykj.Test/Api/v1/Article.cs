﻿using Sykj.Infrastructure;
using Sykj.IServices;
using Sykj.Web.Api.v1;
using System;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Sykj.ViewModel;
using Sykj.Components;

namespace Sykj.Test.Api.v1
{
    public class Article
    {
        IServiceProvider _serviceProvider;
        private readonly IContent _content;
        ArticleController _articleController;

        public Article()
        {
            _serviceProvider = Factory.BuildServiceProvider();
            _content = _serviceProvider.GetService<IContent>();
            _articleController = new ArticleController(_content);//实例化
        }

        //// <summary>
        /// 文章列表 bjg
        /// </summary>
        /// <param name="top">条数</param>
        /// <param name="classId">栏目Id</param>
        /// <returns></returns>
        [Theory]
        [InlineData(5, 78)]
        [InlineData(5, 79)]
        public void List(int top, int classId)
        {
            ApiResult result = _articleController.List(top, classId);
            Assert.True(result.success);
        }

        /// <summary>
        /// 文章详情
        /// </summary>
        /// <param name="id">主键</param>
        [Theory]
        [InlineData(5)]
        public void Detail(int id)
        {
            ApiResult result = _articleController.Detail(id);
            Assert.True(result.success);
        }

        /// <summary>
        /// 文章列表分页
        /// </summary>
        /// <param name="pageIndex">起始页</param>
        /// <param name="pageSize">页大小</param>
        /// <param name="classId">栏目Id</param>
        [Theory]
        [InlineData(1, 5, 12)]
        [InlineData(2, 5, 12)]
        public void PageList(int pageIndex, int pageSize, int classId)
        {
            ApiResult result = _articleController.PageList(pageIndex, pageSize, classId);
            Assert.True(result.success);
        }
    }
}
