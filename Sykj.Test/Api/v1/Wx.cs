﻿using Sykj.Infrastructure;
using Sykj.IServices;
using Sykj.Web.Api.v1;
using System;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Sykj.ViewModel;
using Sykj.Components;
using Microsoft.Extensions.Logging;

namespace Sykj.Test.Api.v1
{/// <summary>
 /// 微信登录测试
 /// </summary>
    public class Wx : IDisposable
    {
        IServiceProvider _serviceProvider;
        IUsers _users;
        IUserMember _userMember;
        private readonly ICacheService _cacheService;
        WxController _wxController;
        ILogger<WxController> _logger;

        /// <summary>
        /// 构造方法
        /// </summary>
        public Wx()
        {
            _serviceProvider = Factory.BuildServiceProvider();
            _users = _serviceProvider.GetService<IUsers>();
            _userMember = _serviceProvider.GetService<IUserMember>();
            _cacheService = _serviceProvider.GetService<ICacheService>();
            _logger = _serviceProvider.GetService<ILogger<WxController>>();
            _wxController = new WxController(_users, _userMember, _cacheService, _logger);//实例化
        }

        /// <summary>
        /// 获得数据测试
        /// </summary>
        /// <param name="code"></param>
        /// <param name="encryptedData"></param>
        /// <param name="iv"></param>

        [Theory]
        [InlineData("033T5Yra0lX4jz1lMtua0bjcsa0T5YrT", "hRBNa92kD8C6YaFrEpo6Tg+hMXf9rAUjdXopdBI4Zv3LVyD05X1fsLdkQkC7qw2anMW0nNHBDdwdX0ap0aIoXIosZOGbf2dakbUgHy0aRP5oMrI9WcFtSBIdEtqtqa9A+ECX7my9ZLZUkaP7AzezrYxUFnpDD42fMrMc/UbGFmsz9QXNGwOU1VgFOj0QOP6LxCUV9pZ4Nf/uf2JI3XgJEZARPMtOSMSIKCXjyYeZAM/4HvqD4eAdjSgZVj9wpIrbMQ8LIqsL9Jx3p7wAU4tYONyYj0Lqz3tNmQHPSqwC8g9UozWbG3JttPzIqrVDhgD/CaFEm6A8YOz72l8Q1oy8XgHjUjIVU3HPGuzW9nscCXEEhjdgNWPpwRgXEDtgN9cY1bOAdC5E4g9QSOc1jP0aYCmvTZ0ZK4ZT3fKDPskgpcgXERZuJfk3EDz9RInX/BrjZeZhwvL5FSMWn3zeEJ20yErXtWAdNk55hJiPyqggRW0=", "v5kLkYhtRcV1SjiUV6VKaQ==")]
        public void GetUnionId(string code, string encryptedData, string iv)
        {
            var model = _wxController.GetUnionId(code, encryptedData, iv);
        }

        /// <summary>
        /// 小程序注册 测试
        /// </summary>
        /// <param name="code"></param>
        /// <param name="encryptedData"></param>
        /// <param name="iv"></param>
        /// <param name="nickName"></param>
        /// <param name="avatarUrl"></param>
        /// <param name="unionId"></param>
        [Theory]
        [InlineData("033T5Yra0lX4jz1lMtua0bjcsa0T5YrT", "hRBNa92kD8C6YaFrEpo6Tg+hMXf9rAUjdXopdBI4Zv3LVyD05X1fsLdkQkC7qw2anMW0nNHBDdwdX0ap0aIoXIosZOGbf2dakbUgHy0aRP5oMrI9WcFtSBIdEtqtqa9A+ECX7my9ZLZUkaP7AzezrYxUFnpDD42fMrMc/UbGFmsz9QXNGwOU1VgFOj0QOP6LxCUV9pZ4Nf/uf2JI3XgJEZARPMtOSMSIKCXjyYeZAM/4HvqD4eAdjSgZVj9wpIrbMQ8LIqsL9Jx3p7wAU4tYONyYj0Lqz3tNmQHPSqwC8g9UozWbG3JttPzIqrVDhgD/CaFEm6A8YOz72l8Q1oy8XgHjUjIVU3HPGuzW9nscCXEEhjdgNWPpwRgXEDtgN9cY1bOAdC5E4g9QSOc1jP0aYCmvTZ0ZK4ZT3fKDPskgpcgXERZuJfk3EDz9RInX/BrjZeZhwvL5FSMWn3zeEJ20yErXtWAdNk55hJiPyqggRW0=", "v5kLkYhtRcV1SjiUV6VKaQ==", "", "", "")]
        public void Register(string code, string encryptedData, string iv, string nickName = "", string avatarUrl = "", string unionId = "")
        {
            var mode = _wxController.Register(code, encryptedData, iv, nickName, avatarUrl, unionId);
        }

        public void Dispose()
        {
            //
        }
    }
}
